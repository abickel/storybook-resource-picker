$(document).ready(function () {
    window.BIMstoryList = null;
    loadStories();
});
$(document).on('click', '#story_select', function () {
});
$(document).on('change', '#story_select', function () {
    if (this.value == 'default') {
        this.classList.add('default');
        return false;
    }
    this.classList.remove('default');
    initializeStorybookPlayer(this.value);
    showDownloadLink(this.value);
});
$(document).on('click', '#storybook-pdf-link', function (e) {
    e.preventDefault();
    e.stopPropagation();
    var location = this.href;
    window.open(location);
    return false;
});
$(document).on('click', '.audio-playback-toggle-control', function () {
    var img = document.getElementById('audio-playback-icon');
    var basUrl = img.dataset.baseUrl;
    var isMuted = img.classList.contains('muted');
    var icon = isMuted ? "audio_k_5.svg" : "mute_k_5.svg";
    img.src = basUrl + icon;
    if (isMuted)
        img.classList.remove('muted');
    else
        img.classList.add('muted');
    var audio_player = document.getElementById('bim-f-audio-player');
    audio_player.volume = isMuted ? 1 : 0;
});
function loadStories() {
    var jsonUrl = "https://static.bigideasmath.com/protected/content/storybook/resources/json/stories.json";
    $.ajax({
        url: jsonUrl,
        method: "GET",
        dataType: "json"
    })
        .done(function (response) {
        var temp_stories = response.storyboards.sort(function (a, b) { return a.sort_index - b.sort_index; });
        window.BIMstoryList = response.storyboards;
        var selectbox = document.getElementById('story_select');
        selectbox.innerHTML = "";
        var defaultOpt = document.createElement('option');
        defaultOpt.value = "default";
        defaultOpt.classList.add('default');
        defaultOpt.innerText = "Select a story";
        defaultOpt.id = "default-select-option";
        selectbox.appendChild(defaultOpt);
        for (var i = 0; i < window.BIMstoryList.length; i++) {
            var opt = document.createElement('option');
            var story = window.BIMstoryList[i];
            if (story.key && story.key.length) {
                opt.value = story.key;
                var _title = story.resource_title ? story.resource_title : story.key;
                opt.innerHTML = _title;
                selectbox.appendChild(opt);
            }
        }
    })
        .fail(function (xhr) {
        console.log('XHR failure', xhr);
    });
}
function capitalize(string) {
    var chars = string.split("");
    var cap = chars[0].toUpperCase();
    for (var i = 1; i < chars.length; i++)
        cap = cap + chars[i].toLowerCase();
    return cap;
}
function initializeStorybookPlayer(storyKey) {
    document.getElementById('storybook_content').innerHTML = "";
    document.getElementById('story_select').disabled = true;
    var initOpts = {
        storyboard_key: storyKey,
        language: 'e',
        image_location: "https://static.bigideasmath.com/protected/content/storybook/resources/images",
        json_location: "https://static.bigideasmath.com/protected/content/storybook/resources/json/stories.json",
        allow_tts: true,
        show_playback_controls: true,
        target_id: "storybook_content"
    };
    BIMStories.storyboard = new BIMStories.BimStoryBoard(initOpts);
    BIMStories.storyboard.show_audio_controls = true;
    BIMStories.storyboard.show_language_controls = true;
    injectPlaybackControls();
    setTimeout(function () {
        document.getElementById('story_select').disabled = false;
    }, 500);
}
function showDownloadLink(storyKey) {
    var container = document.getElementById('resource-link-container');
    container.classList.remove('hidden');
    var link = document.getElementById('storybook-pdf-link');
    link.removeAttribute('disabled');
    var baseUrl = "https://static.bigideasmath.com/protected/content/storybook/resources/pdfs/";
    link.href = baseUrl + storyKey + ".pdf";
}
function render_option_buttons() {
    var sibling = document.getElementsByClassName('story-select-container')[0];
    var _self = document.getElementById('language_toggle');
    if (!!_self)
        return;
    var lang_toggle = document.createElement('div');
    var eng = document.createElement('div');
    eng.id = "e-opt";
    var spn = document.createElement('div');
    spn.id = "s-opt";
    lang_toggle.id = "language_toggle";
    eng.classList.add('lang-option', 'lang-option-selected');
    eng.innerHTML = "English";
    eng.value = "e";
    spn.classList.add('lang-option');
    spn.innerHTML = "Espa&#241;ol";
    spn.value = "s";
    lang_toggle.appendChild(eng);
    lang_toggle.appendChild(spn);
    sibling.parentNode.appendChild(lang_toggle);
}
function injectPlaybackControls() {
    var audioBtn = document.getElementById('playback-toggle-button');
    if (!!audioBtn)
        return;
    audioBtn = document.createElement('button');
    audioBtn.classList.add('audio_k_5');
    audioBtn.classList.add('audio-playback-toggle-control');
    audioBtn.id = "playback-toggle-button";
    var img = document.createElement('img');
    img.id = "audio-playback-icon";
    img.classList.add('audio-playback-icon');
    img.src = "https://static.bigideasmath.com/includes/dc-audio-playback/images/audio_k_5.svg";
    img.dataset.baseUrl = "https://static.bigideasmath.com/includes/dc-audio-playback/images/";
    audioBtn.appendChild(img);
    var parent = document.getElementById('story_select').parentElement;
    parent.appendChild(audioBtn);
}
function bubbleSort(array, key) {
    var swapped;
    do {
        swapped = false;
        console.log('iter');
        for (var i = 0; i < array.length; i++) {
            if (array[i] && array[i + 1] && array[i][key] > array[i + 1][key]) {
                var temp = array[i];
                array[i] = array[i + 1];
                array[i + 1] = temp;
                swapped = true;
            }
        }
    } while (swapped);
    return array;
}
//# sourceMappingURL=script.js.map